# Membres
	- Luc LIGER
	- Joffrey DALENCON


# Lien dépot git

## HTTPS:
git clone https://dalencon1u@bitbucket.org/dalencon1u/lpcisiie_dalencon_liger.git

## SSH:
git clone git@bitbucket.org:dalencon1u/lpcisiie_dalencon_liger.git



# Fonctionnalités

* Typographie, boutton, lien, jumbotron, tableau, liste, alerte, titre/soustitre, header, fil d'ariane, pagination.
* Grille responsive, 3 tailles (lg , md et sm).



# MANUAL

Il faut construire son site avec la grille pour que le contenu soit reponsive.

il faut utiliser les 3 classes des 3 tailles :

* lg : correspond aux grands écrans (ordinateur, télévision, ..)
* md : correspond aux écrans moyens (tablette, ..)
* sm : correspond aux pétits écrans (téléphones, montres, ..)

Chaque grille de chaque taille est de base découpél en 8 colonnes. Pour changer le nombre de colonne il suffit d'aller dans le scss/main et de changer la valeur de nbcol.

On peut également regrouper des colonnes entres elles (exemple : lg2 correspond à 2x lg1 + la marge entre).

On peut également décaller la colonne (exemple : offset-lg1 décale la colonne en taille lg de 1 colonne)


Le code se présente ainsi :


	<div class="container"> // obligatoire

		<div class="lg6 offset-lg1 md6 offset-md1 sm8"> // on met les tailles souhaitées

			.

			.

			.

		</div>

	</div>
